#resource aws_iam_role iam_for_s3_to_sqs_lambda


resource "aws_iam_role" "iam_for_s3_to_sqs_lambda" {
  name = var.s3_to_sqs_lambda_role_name
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "lambda.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy" "lambda_logging_for_s3_to_sqs" {
  name = "lambda_logging_for_s3_to_sqs"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "s3:*",
        "Resource": "${aws_s3_bucket.job_offers_bucket.arn}/*"
      }
    ]
  })
}

resource "aws_iam_role_policy" "iam_policy_for_s3" {
  name = "iam_policy_for_s3"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sqs:SendMessage",
        "Resource": "${aws_sqs_queue.job_offers_queue.arn}"
      }
    ]
  })
}





resource "aws_iam_role" "sqs_to_dynamo_lambda_role" {

  name = var.sqs_to_dynamo_lambda_name

  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "lambda.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_policy" "sqs_to_dynamo_policy" {
  name        = "sqs_to_dynamo_policy"
  description = "Policy for allowing Lambda to interact with SQS"

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ],
        "Resource": aws_sqs_queue.job_offers_queue.arn
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "sqs_to_dynamo_attachment" {
  policy_arn = aws_iam_policy.sqs_to_dynamo_policy.arn
  role       = aws_iam_role.sqs_to_dynamo_lambda_role.name
}




resource "aws_iam_role_policy" "dynamodb_put_policy" {
  name = "dynamodb_put_policy"
  role = aws_iam_role.sqs_to_dynamo_lambda_role.name  # Assurez-vous que le nom du rôle correspond à celui que vous avez créé

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "dynamodb:PutItem",
        "Resource": aws_dynamodb_table.job-table.arn  # Assurez-vous que le nom de la table DynamoDB correspond à celui que vous avez créé
      }
    ]
  })
}


# resource aws_iam_role_policy iam_policy_for_sqs_receiver


# resource aws_iam_role iam_for_job_api_lambda


# resource aws_iam_role_policy iam_policy_for_dynamodb


# resource aws_iam_role_policy lambda_logging_for_job_api
#resource "aws_iam_role_policy" "lambda_logging_for_job_api" {
#  name   = "lambda_logging_for_job_api"
#  role   = aws_iam_role.iam_for_job_api_lambda.id
#  policy = file("files/log_policy.json")
#}

# resource aws_iam_role_policy_attachment lambda_exec_policy_attach

