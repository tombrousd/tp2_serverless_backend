# resource aws_s3_bucket s3_job_offer_bucket

resource "aws_s3_bucket" "job_offers_bucket" {
  bucket = "serverlesstomtp"
  acl    = "private"
}


# resource aws_s3_object job_offers

resource "aws_s3_bucket_object" "job_offers_object" {
  bucket = aws_s3_bucket.job_offers_bucket.id
  key    = "job_offers/"
  source = "/dev/null"
}

# resource aws_s3_bucket_notification bucket_notification

resource "aws_s3_bucket_notification" "lambda_trigger" {
  bucket = aws_s3_bucket.job_offers_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_bucket_object.job_offers_object.id
    filter_suffix       = ".csv"
  }
}







