data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

resource "aws_lambda_function" "s3_to_sqs_lambda" {
  filename         = data.archive_file.empty_zip_code_lambda.output_path
  function_name    = var.s3_to_sqs_lambda_name
  role             = aws_iam_role.iam_for_s3_to_sqs_lambda.arn
  handler          = "lambda.handler"

  runtime          = "nodejs18.x"
  timeout          = 900
  memory_size      = 512
   # Variables d'environnement
  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.url
    }
  }
}

resource "aws_lambda_permission" "allow_execution_from_s3" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.job_offers_bucket.arn

  # Dépendance sur la création de la fonction Lambda
  depends_on = [aws_lambda_function.s3_to_sqs_lambda]
}

############# partie 2 #####################



resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  filename      = data.archive_file.empty_zip_code_lambda.output_path
  function_name = "sqs_to_dynamo_lambda"
  role          = aws_iam_role.sqs_to_dynamo_lambda_role.arn
  handler       = "lambda.handler"
  source_code_hash = data.archive_file.empty_zip_code_lambda.output_path
  runtime       = "nodejs18.x"
  timeout       = 30
  memory_size   = 512

  # Environment variables
  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}

resource "aws_lambda_permission" "sqs_to_dynamo_lambda_permission" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.arn
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.job_offers_queue.arn
}



# resource aws_lambda_permission allow_bucket


# resource aws_lambda_function sqs_to_dynamo_lambda


# resource aws_lambda_permission allow_sqs_queue


# resource aws_lambda_function job_api_lambda



# resource aws_lambda_permission allow_api_gw

