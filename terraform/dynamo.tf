resource "aws_dynamodb_table" "job-table" {
  name           = var.dynamo_job_table_name  # Remplacez par le nom souhaité de la table
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "city"
    type = "S"
  }

  hash_key = "id"    # Clé de hachage
  range_key = "city" # Clé de tri (range key)
}
