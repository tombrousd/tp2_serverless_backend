# resource aws_sqs_queue job_offers_queue

resource "aws_sqs_queue" "job_offers_queue" {
  name                      = var.sqs_job_queue_name
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.job_offers_queue_deadletter.arn
    maxReceiveCount     = 4
  })

  tags = {
    Environment = "production"
  }
}

# resource aws_sqs_queue job_offers_queue_deadletter

resource "aws_sqs_queue" "job_offers_queue_deadletter" {
    name = var.sqs_job_dead_letter_queue_name
}

# resource aws_lambda_event_source_mapping sqs_lambda_trigger

resource "aws_lambda_event_source_mapping" "sqs_to_dynamo_lambda_event_source_mapping" {
  event_source_arn = aws_sqs_queue.job_offers_queue.arn
  function_name    = aws_lambda_function.sqs_to_dynamo_lambda.function_name
}
